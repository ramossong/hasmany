# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
HasMany::Application.config.secret_key_base = '2c8904b2616ebd832a1b99d160f833c36d7cb1a4eafef4f3a1ac3df5f0698466b92f48ae96f15e1e5f09076f79b8d4e5a5fac46dba3c649fe6c8fedb6657a7e4'
