# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

   patient1 = Patient.create(name: 'John', street_address: '123 Elm')
   patient2 = Patient.create(name: 'Jill', street_address: '123 Oak')
   patient3 = Patient.create(name: 'Jeff', street_address: '123 Walnut')
   patient4 = Patient.create(name: 'Jack', street_address: '123 Pine')
   patient5 = Patient.create(name: 'James', street_address: '123 Gooseberry')
   specialist1 = Specialist.create(name: 'Bill', specialty: 'Brain')
   specialist2 = Specialist.create(name: 'Ben', specialty: 'Ear, Nose, and Throat')
   specialist3 = Specialist.create(name: 'Brian', specialty: 'Foot')
   insurance1 = Insurance.create(name: 'BobCo Insurance', street_address: '725 Main Street')
   insurance2 = Insurance.create(name: 'State Farm Insurance', street_address: '124 1st Street')
   insurance3 = Insurance.create(name: 'Geico Insurance', street_address: '836 2nd Street')
   insurance4 = Insurance.create(name: 'USAA Insurance', street_address: '872 3rd Street')
   insurance5 = Insurance.create(name: 'Progressive Insurance', street_address: '993 4th Street')
   appt1 = Appointment.create(specialist_id: 1, patient_id: 1, complaint: 'My Brain Hurts', appointment_date: '2014-09-29')
   appt2 = Appointment.create(specialist_id: 3, patient_id: 1, complaint: 'I broke my foot', appointment_date: '2014-09-30')
   appt3 = Appointment.create(specialist_id: 2, patient_id: 2, complaint: 'I think I have strep', appointment_date: '2014-10-04')
   appt4 = Appointment.create(specialist_id: 3, patient_id: 2, complaint: 'Smashed my toe with a hammer', appointment_date: '2014-10-05')
   appt5 = Appointment.create(specialist_id: 2, patient_id: 3, complaint: 'I have a ruptured ear drum', appointment_date: '2014-10-07')
   appt6 = Appointment.create(specialist_id: 1, patient_id: 3, complaint: 'I keep losing feeling in my left sisafsdfaccaxxxxxxxxxx', appointment_date: '2014-10-09')
   appt7 = Appointment.create(specialist_id: 3, patient_id: 4, complaint: 'I think i have plantar fasciitis', appointment_date: '2014-10-13')
   appt8 = Appointment.create(specialist_id: 1, patient_id: 4, complaint: 'Do I have an appointment here? I cannot remember.', appointment_date: '2014-10-15')
   appt9 = Appointment.create(specialist_id: 3, patient_id: 5, complaint: 'I have accidentally cut some toes off working on building a deck', appointment_date: '2014-10-24')
   appt10 = Appointment.create(specialist_id: 2, patient_id: 5, complaint: 'I was punching some dude and he broke my nose.', appointment_date: '2014-10-26')
